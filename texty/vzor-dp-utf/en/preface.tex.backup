\chapter{Introduction}
%\addcontentsline{toc}{chapter}{Introduction}

\section{Motivation and goals}

Dynamic state estimation is an interesting and intensively reseached topic due to its variety of applications. The task is to (formally) describe a system that evolves in time and its state is not directly observable. Then we can be interested in the true state value in a certain time given some measurements or make predictions about the future. Such systems can describe biological processes, economy, robotics or be used in computer graphics.

We focus on the field of robotics and use real data from unmanned aerial vehicles (UAVs) also known as drones. This kind of robot becomes increasingly popular these days. There are a few commercial models available (\cite{ar-drone}) and many low-cost home-made ones. For autonomous control of a robot, state estimation is crucial. A state typically consists of the position including rotation angles and the current speed. These values can be measured by sensors, but they usually provide noised data. Moreover, some of the sensors may be unavailable. 

The most common approach is to maintain a probabilistic distribution of the current state estimate and update it according to the model of the system dynamics and measurement model. These models or their parameters may not be known and learning of them is referred to as {\em system identification}.

Our goal is to design and implement a probabilistic model for state estimation of a particular UAV. We focus on the tasks where some of the sensors are not available and their values need to be predicted, which requires more accurate model. We use EM algorithm to learn the parameters, Kalman filter and Particle filter to provide estimates. We evaluate our models on separate datasets using likelihood and RMSE for predicting the missing values.

We design the methods to be general enough and easily applicable for another types of robots. We test our implementation of Particle filter on a

\section{Structure of the thesis}

Chapter \ref{chap:theory} starts with a description of the dynamic state estimation problem in general. It introduces three main tasks--filtering, smoothing and learning the parameters.
It briefly summarizes the most common approaches to the filtering problem and then focuses on two of them--Kalman filter and Particle filter. These algorithms are described to a level of detail sufficient for implementation. A special part is devoted to the task of learning the parameters. For this task, EM algorithm is presented. Its application for both Kalman filter and Particle filter is discussed.

Chapter \ref{chap:application} describes our approach to state estimation of UAV. It first summarizes the basic approaches of the system identification used by \cite{cvut} and continues with application of the methods from the previous chapter for this specific data. Finally, a few evaluation experiments compare the methods we used and the final results are presented.

More details of our implementation are then discussed in chapter \ref{chap:implementation}. 

In chapter \ref{chap:other-datasets}, our analysis of other robotics datasets is presented. The first dataset is from a special experiment Vicon that enables to analyze measurement model more precisely. The second is a ground robot dataset, where our implementation of Particle filter is used to estimate the position of the robot based on GPS and compass measurements.