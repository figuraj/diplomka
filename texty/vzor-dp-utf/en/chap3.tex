\chapter{Other datasets} \label{chap:other}

\section {Vicon data} \label{sec:vicon}

We received data from an experiment called Vicon. It was performed using a special system for tracking objects. Three drones were flying in a room and a set of external cameras were recording. The system can provide very accurate positions of each drone for each time step. 

Moreover, each drone has its own camera to detect other drones. This camera system can detect black circle on a white background. Each drone carries this sign hung on itself. The detection work as follows. It starts scanning the camera image from the position of previously detected object. As soon as it finds the object, it calculates its distance estimation based on its size and relative angle. It outputs a new record with the distance estimation and x and y coordinates of the object on the camera picture. This record is then joined with the drone's real position obtained by the Vicon system. 

We have a file consisting of such records for each drone. The main data fields of a record were the following:

\begin{itemize}
\item \bf vicon\_time \rm - system time in ms
\item \bf relative\_dist\_cam \rm - estimated distance of an object seen by a drone's camera
\item \bf x\_coord\_cam \rm - x coordinate where the object was seen by a drone's camera
\item \bf y\_coord\_cam \rm - y coordinate where the object was seen by a drone's camera
\item \bf x\_vicon \rm - x coordinate where Vicon detected the drone
\item \bf y\_vicon \rm - y coordinate where Vicon detected the drone
\item \bf z\_vicon \rm - z coordinate where Vicon detected the drone.
\end{itemize}

We do not know which drone was detected by the camera. However, we can estimate that by joining the files. For each drone we create a joint log by joining each its record with the closest (in time) records of both other drones. Let the drones be called 1, 2 and 3. In a joint log, the other drones are denoted A and B, A is the one with the smaller name (for instance in joint log of drone 3, drone 1 is A and drone 2 is B). We compute the real distance of A and B from the current drone (based on vicon coordinates). We get a joint record like this:

\begin{itemize}
\item \bf vicon\_time \rm
\item \bf vicon\_time\_A \rm
\item \bf vicon\_time\_B \rm
\item \bf dist\_cam \rm
\item \bf dist\_A \rm
\item \bf dist\_B \rm.
\end{itemize}

First, we checked if joining the data on time is valid. We calculated time match errors, that is the differences between $vicon\_time$, $vicon\_time\_A$ and $vicon\_time\_B$ for each record. On average, the time match error is 100ms, but most of them are around 10ms. Therefore, we can consider this matching as valid.

Now, we can plot the data for each drone.

\medskip
\begin{figure}[ht]
\centerline{\mbox{\includegraphics[width=160mm]{../img/vicon/vicon.png}}}
\caption{Real distances of drones and the estimates in time.}
\end{figure}
\medskip

We can see that the detection module underestimates the distances by a constant plus a small random noise. To plot a histogram of measurement errors, we have to find out which drone was most probably seen. Let $err\_A$ be the distance error if the drone sees the drone A. That is $dist\_cam - dist\_A$. Similarly for seeing drone B. Then we define the distance error as the one with smaller absolute value from $err\_A$ and $err\_B$. Note that the error is signed as we want to know also if the measurements are underestimated or overestimated. This definition of the distance error is not perfect, because we cannot be sure which drone was actually seen. However, it should be sufficient enough to design the measurement model. The distribution of the real distances is a Gaussian centered in a point with a fixed offset from the measured value.

\medskip
\begin{figure}[ht]
\centerline{\mbox{\includegraphics[width=160mm]{../img/vicon/hist.png}}}
\caption{Histograms of distance errors for each drone.}
\end{figure}
\medskip

We also tried to learn a model that would predict the error from camera coordinates (for instance, objects seen on the edges of the camera view have less accurate distance calculations). However, there was not enough data to distinguish various regions of the camera image. Almost all the records were from the center of the image.

\section {Ground robots}\label{sec:ground-robot}

For testing our implementation of Particle filter on a different type of robot, we used data from a ground robot. Our task was to filter the robot's position using its control signals, GPS and compass measurements. The robot was moving in a town with a known map of streets. Using that, true location was provided for us. We could learn the parameters of the model from that and we used it as a measure of accuracy of our filter. 

\subsection{Problem description}

The state of the robot is described by its position and orientation. The position is measured in meters relative to a certain point (0, 0) in the town and the orientation is in radians. Control signals consist of $alpha$, $dist$ and $beta$. A single control signal means that the robot is requested to rotate with angle $alpha$, then move forward $dist$ meters and then rotate with angle $beta$. Based on this we have the following (nonlinear) transition model:

\begin{equation}
\begin{gathered} 
x_{t+1} = x_t + cos(angle_t + alpha_t) dist_t\\
y_{t+1} = y_t + sin(angle_t + alpha_t) dist_t\\
angle_{t+1} = y_t + alpha_t + beta_t.
\end{gathered} \label{eq:ground-transition}
\end{equation} 

The robot receives two kinds of measurements, GPS and compass. The measurements are asynchronous, they do not come together, nor together with the control signals. There is approximately 10 compass measurements and 10 control signals for a single GPS measurement. Moreover, it takes some time to initialize the GPS sensor, therefore there is no GPS data at the beginning (first 5\%). 

Compass shows the orientation of the robot (corresponds to the state variable $angle$) in radians. GPS shows the latitude and longitude that can be easily converted to our metric system during the preprocessing step\footnote{For the map projection details see \cite{projection_1}. In MATLAB implementation, we used a function from \cite{projection_2}}.  

The data also contains the expected accuracy for each measurement of both GPS and compass, but the analysis showed that these values are useless.

\subsection{Data analysis}

When true state values are provided, we do not need to use EM algorithm as its estimation phases can never produce better estimate than we already have. However, we should try to analyze the data and perform a single maximization step based on that.

\subsubsection{GPS}

We plot the real position and GPS measurements first as a planar image to see the trajectory in 2D (Figure \ref{fig:ground-position}). Then we plot the distance error in time (Figure \ref{fig:ground-position}). GPS shows wrong data in the middle part. For the rest of the time, it provides a reasonable estimate of the position. \ref{fig:ground-position} also shows that expected GPS accuracy does not correspond to the real error. Finally, we generate a histogram of GPS error (Figure \ref{fig:ground-position}). If we ignore the cases where the GPS is absolutely wrong, the error can be approximated by a normal distribution.

Our first approach was to set the variance large enough so that the particles with the correct position would have non-zero probability in case of GPS being wrong. However, setting the variance low (5m) as it really is in case of GPS working correctly leads to better performance. This approach works, because if the GPS is working, it provides a better estimation and if it stops working, all the particles have zero weights and are sampled just according to the control signals. This assumes that the first sequence of GPS measurements is correct and that the sequence of wrong measurements is not too long (control signals are sufficient to lead the particles without correction). These properties hold in our data.

\medskip
\begin{figure}[ht]
\centerline{\mbox{\includegraphics[width=160mm]{../img/ground/analysis/position.png}}}
\caption{Real trajectory in 2D with measurements of GPS. The beginning of the trajectory is not plotted as there is no GPS measurement available.}
 \label{fig:ground-position}
\end{figure}

\begin{figure}[ht] 
\centerline{\mbox{\includegraphics[width=160mm]{../img/ground/analysis/gps_err.png}}}
\caption{GPS distance error in time with its expected accuracy (given as radius of expected position). We see it does not correlate with the real error.}
\label{fig:gps-err}
\end{figure}

\begin{figure}[ht] 
\centerline{\mbox{\includegraphics[width=160mm]{../img/ground/analysis/gps_err_hist.png}}}
\caption{Histograms of the GPS error.}
\label{fig:gps-hist}
\end{figure}
\medskip

\subsubsection{Compass}

Compass values are given in radians. Some of the values from compass sensor are greater than $2\pi$ so we have to compute the modulo value.  We also have to be careful with comparing angles near 0 (0.1 is close to 6.2 in radians).

By plotting the real angles and compass values, we can propose the measurement model for compass. Figures \ref{fig:compass} and \ref{fig:compass-hist} show that there are 5 separate regions of compass values, each with a different offset from the real value. Based on this, we represent the measurement model with 5 normal distributions. For example, measurements for real angles between 4.5 and 6 radians are distributed normally with mean $angle - 0.25$. Histogram suggests low variances about 0.15, but higher variances (e.g 1) work better in practice.

Identifying such regions may seem as overfitting. However, there is a recurring region in our data, time 1500 to 2500 and then 3500 to 4500, and both of the sequences have the same characteristics. 

Representing the compass error distribution by a single gaussian requires either a fixed averaged offset (0.5 radian) or higher variance (2 or 3 radians).

\medskip
\begin{figure}[ht] 
\centerline{\mbox{\includegraphics[width=160mm]{../img/ground/analysis/compass.png}}}
\caption{Compass values and real orientation in time. The values are in radians, values between $0$ and $2$ were moved to $[2\pi, 2\pi + 2]$} to improve the plot readability.
\label{fig:compass}
\end{figure}

\begin{figure}[ht]
\centerline{\mbox{\includegraphics[width=160mm]{../img/ground/analysis/compass_hist.png}}}
\caption{Histogram of compass error. The error is signed, positive error means greater measurement than real value.}
 \label{fig:compass-hist}
\end{figure}
\medskip

\subsubsection{Transition model}

The deterministic form of the transition model is described by equations \eqref{eq:ground-transition}. There are two main method how to introduce uncertainty to this model. We can either take the deterministic values of the new position and add a random noise to each of them (x, y and angle), or add the noise to the control signals and calculate the new position using these noised signals. For both of the method, we can calculate the appropriate noise distributions using the real values. The first method is more straight-forward but less plausible.

Using the first approach, we compare the real positions to the positions calculated by our deterministic model. Figure \ref{fig:transition-hist} shows the error of the model. The error is not Gaussian. The error is very close to zero in significantly more cases. We propose a natural hypothesis that the error is dependant on the control signals. The greater the distance to move or angle to rotate, the greater is the expected error. The simplest approach is to handle the control signals with zero distance or zero rotation separately and not to include any noise in these cases. More generally, we try to learn the expected error given the control signals. We use a linear regression to fit the parameters. The error plots suggest that the error may grow quadratically with the distance, therefore we added the distance square to the predictors. Considering also the current angle did not prove useful. Finally, we found the parameters that predict the absolute value of the error of x and y coordinate given $dist$ and $dist^2$ and the error of angle given $|alpha + beta|$. We intend to use these predicted values as the variances in the transition model. However, for better performance of the particle filter, multiplying the predicted values by some hand-tuned constants is necessary.

In conclusion, each transition step computes the deterministic values of the new position according to \eqref{eq:ground-transition} and adds a random noise distributed normally with zero mean and variance calculated based on the current control signals.

\medskip
\begin{figure}[ht] 
\centerline{\mbox{\includegraphics[width=160mm]{../img/ground/analysis/transition_hist.png}}}
\caption{Histograms of the transition model error.}
\label{fig:transition-hist}
\end{figure}
\medskip

\subsection{Asynchronous measurements}

In the standard filtering task, we assumed we receive control signals and all measurements at once and in a fixed sample rate. In a more general and more realistic setting, the measurements are received asynchronously. That is, for a given time there is a single message from one of the sensors or a control signal. In our data we assume that no control signal is missing and the robot moves only according to the control signals. There is no timestamp in either measurements or control signals.

We used a modification of particle filtering algorithm that follows a simple set of rules. Each input record can be either control signal or a measurement. If it is a control signal, we perform a transition step for each particle and output our state estimation. If it is a measurement, we perform a measurement step. That is, we assign a weight to each particle according to the model of the corresponding sensor (GPS or compass) and resample.

\subsection{Results}

Figure \ref{fig:ground-performance} shows the best performance of our particle filter. Compared to the real positions, the filter has mean error of approximately 6 meters. It also depends on the initialization. We use two types of initialization. The first (on the figure) initializes the particle randomly a few meters around the real start position. The second initializes the particles into a greater area, a rectangle covering all future GPS measurements. That is 250 times 100 meters.

In conclusion, there are a few things that improved the performance the most. First, setting the GPS variance low instead of setting it as a variance that includes absolutely wrong measurements. Second, realizing that the orientation values have to be compared in angle distance sense. Third, analyzing the compass error and finding the best offset. Fourth, identifying the transition error dependance on the control signals. 

\medskip
\begin{figure}[ht] 
\centerline{\mbox{\includegraphics[width=160mm]{../img/ground/result.png}}}
\caption{The best filtering performance of our methods, 2D trajectory.}
\label{fig:ground-performance}
\end{figure}
\medskip
