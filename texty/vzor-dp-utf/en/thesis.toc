\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation and goals}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Structure of the thesis}{3}{section.1.2}
\contentsline {chapter}{\numberline {2}Theoretical background}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Problem definition and notations}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Notation remarks}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Dynamic systems description}{5}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}The tasks}{6}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Filtering methods overview}{7}{subsection.2.1.4}
\contentsline {subsubsection}{Kalman filter}{7}{section*.2}
\contentsline {subsubsection}{Extended Kalman filter and Unscented Kalman filter}{7}{section*.3}
\contentsline {subsubsection}{Grid-based methods}{7}{section*.4}
\contentsline {subsubsection}{Particle filters}{8}{section*.5}
\contentsline {section}{\numberline {2.2}Kalman filter}{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Definition}{8}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Algorithm}{8}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Kalman smoother}{9}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}EM Algorithm}{10}{subsection.2.2.4}
\contentsline {subsubsection}{Likelihood}{10}{section*.6}
\contentsline {subsubsection}{Log-likelihood for Kalman filter}{11}{section*.7}
\contentsline {subsubsection}{Maximizing of the log-likelihood}{12}{section*.8}
\contentsline {subsubsection}{Expected likelihood}{13}{section*.9}
\contentsline {subsubsection}{Algorithm}{14}{section*.10}
\contentsline {section}{\numberline {2.3}Particle filter}{14}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Introduction}{14}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}General algorithm derivation \cite {pf_tutorial}}{14}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Resampling}{16}{subsection.2.3.3}
\contentsline {subsubsection}{Definition}{16}{section*.11}
\contentsline {subsubsection}{Algorithm}{16}{section*.12}
\contentsline {subsubsection}{When to resample}{17}{section*.13}
\contentsline {subsubsection}{Sample impoverishment}{17}{section*.14}
\contentsline {subsection}{\numberline {2.3.4}SIR Particle filter}{17}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Alternative methods}{18}{subsection.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}Particle smoother}{18}{subsection.2.3.6}
\contentsline {subsection}{\numberline {2.3.7}EM Algorithm}{19}{subsection.2.3.7}
\contentsline {subsubsection}{Likelihood and its expectation}{19}{section*.15}
\contentsline {subsubsection}{Nonlinear models}{20}{section*.16}
\contentsline {subsubsection}{Maximizing the mixture of Gaussians measurement model}{20}{section*.17}
\contentsline {subsubsection}{Algorithm overview}{21}{section*.18}
\contentsline {chapter}{\numberline {3}Application for unmanned aerial vehicles}{22}{chapter.3}
\contentsline {section}{\numberline {3.1}Data and tasks}{22}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Data description}{22}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Tasks}{23}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Currently used methods}{24}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Applied methods}{24}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Preprocessing}{25}{subsection.3.2.1}
\contentsline {subsubsection}{Missing values}{25}{section*.19}
\contentsline {subsubsection}{Scaling}{25}{section*.20}
\contentsline {subsection}{\numberline {3.2.2}Dynamic system description}{25}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Learning parameters by linear regression}{27}{subsection.3.2.3}
\contentsline {subsubsection}{Preprocessing by smoothing the data}{27}{section*.21}
\contentsline {subsubsection}{Conclusion}{28}{section*.22}
\contentsline {subsection}{\numberline {3.2.4}Parameters of the model}{29}{subsection.3.2.4}
\contentsline {subsubsection}{Linear models}{29}{section*.23}
\contentsline {subsubsection}{Linear models with penalization}{29}{section*.24}
\contentsline {subsubsection}{Nonlinear extension}{30}{section*.25}
\contentsline {subsection}{\numberline {3.2.5}Noise models}{30}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Filter, smoother and EM algorithm}{31}{subsection.3.2.6}
\contentsline {section}{\numberline {3.3}Experiments}{32}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Evaluation methods}{32}{subsection.3.3.1}
\contentsline {subsubsection}{Introduction}{32}{section*.26}
\contentsline {subsubsection}{Summary of the used evaluation methods}{32}{section*.27}
\contentsline {subsection}{\numberline {3.3.2}Validation parameters}{34}{subsection.3.3.2}
\contentsline {subsubsection}{Training size}{34}{section*.28}
\contentsline {subsubsection}{Validation set distance}{35}{section*.29}
\contentsline {subsection}{\numberline {3.3.3}Experiment $A$ - linear regression learning}{36}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Experiment $B$ - EM algorithm}{39}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Experiment $C$ - extended model with EM algorithm}{41}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Experiment $D$ - Particle filter}{44}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}Final evaluation}{45}{subsection.3.3.7}
\contentsline {subsection}{\numberline {3.3.8}Other experiments}{47}{subsection.3.3.8}
\contentsline {subsubsection}{Scaling}{47}{section*.30}
\contentsline {subsubsection}{Estimating $\kappa $ parameters}{47}{section*.31}
\contentsline {subsubsection}{Measurement offset}{47}{section*.32}
\contentsline {subsubsection}{Adding random noise to particles}{47}{section*.33}
\contentsline {subsubsection}{Mixture of Gaussians model}{47}{section*.34}
\contentsline {subsubsection}{Higher order Markovian system}{48}{section*.35}
\contentsline {section}{\numberline {3.4}Conclusion}{48}{section.3.4}
\contentsline {subsubsection}{Best models description}{48}{section*.36}
\contentsline {subsubsection}{Summary}{49}{section*.37}
\contentsline {chapter}{\numberline {4}Other datasets}{50}{chapter.4}
\contentsline {section}{\numberline {4.1}Vicon data}{50}{section.4.1}
\contentsline {section}{\numberline {4.2}Ground robots}{52}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Problem description}{52}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Data analysis}{53}{subsection.4.2.2}
\contentsline {subsubsection}{GPS}{53}{section*.38}
\contentsline {subsubsection}{Compass}{53}{section*.39}
\contentsline {subsubsection}{Transition model}{54}{section*.40}
\contentsline {subsection}{\numberline {4.2.3}Asynchronous measurements}{55}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Results}{56}{subsection.4.2.4}
\contentsline {chapter}{\numberline {5}Implementation}{61}{chapter.5}
\contentsline {section}{\numberline {5.1}Kalman filter}{61}{section.5.1}
\contentsline {section}{\numberline {5.2}EM algorithm}{61}{section.5.2}
\contentsline {section}{\numberline {5.3}Particle filter}{62}{section.5.3}
\contentsline {subsubsection}{Filter and smoother}{62}{section*.41}
\contentsline {subsubsection}{Asynchronous filter}{63}{section*.42}
\contentsline {subsubsection}{Examples}{63}{section*.43}
\contentsline {subsubsection}{Measurement models}{64}{section*.44}
\contentsline {section}{\numberline {5.4}Evaluation}{64}{section.5.4}
\contentsline {section}{\numberline {5.5}List of scripts}{65}{section.5.5}
\contentsline {chapter}{\numberline {6}Conclusion}{66}{chapter.6}
\contentsline {section}{\numberline {6.1}Summary}{66}{section.6.1}
\contentsline {subsubsection}{UAV application}{66}{section*.45}
\contentsline {subsubsection}{Ground robot dataset}{66}{section*.46}
\contentsline {section}{\numberline {6.2}Future work}{66}{section.6.2}
\contentsline {chapter}{Bibliography}{68}{chapter*.47}
\contentsline {chapter}{\numberline {A}Content of the CD}{70}{appendix.A}
