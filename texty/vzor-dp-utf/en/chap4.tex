\chapter{Implementation} \label{chap:implementation}

We implemented all the methods using {\em MATLAB}. In the following sections, we describe the implementation of Kalman filter and smoother, Particle filter and smoother, EM algorithm and evaluation methods. The last section summarizes the scripts that are prepared for testing these methods.

\section {Kalman filter}

Function \texttt{kalman\_filter(Z, A, H, Q, R, P, x, include\_backward)} serves as Kalman filter if \texttt{include\_backward} parameter is set to \texttt{false} and as Kalman smoother if the parameter is \texttt{true}. \texttt{Z} is a matrix containing all the measurements, each time in a single column. \texttt{A, H, Q, R} are standard Kalman filter parameters (transition and measurement matrices and transition and measurement error covariance matrices). \texttt{P} is the prior covariance and \texttt{x} is the prior state (column) vector.

The function returns the following list: \texttt{\justify[X\_apr, X\_post, X\_smooth, P\_post, P\_smooth, L\_smooth]}. The first 3 output variables are matrices of prior, posterior and smoothed (respectively) state estimates for each time step. \texttt{P\_post} and \texttt{P\_smooth} are the posterior and smoothed covariances for each time step (3 dimensional matrices with the last dimension of time). \texttt{L\_smooth} contains matrices $L$ for each time step, as described in Kalman smoother algorithm (Section \ref{sec:kalman-smoother-alg}). All the output parameters are required as inputs for EM algorithm. If not using this function within EM algorithm, \texttt{X\_post} is the filtering output and \texttt{X\_smooth} is the smoother output and the rest of the output values can be ignored.

Script \texttt{\justify run\_task\_I\_kalman} demonstrates the usage of Kalman filter. The script loads the data from a flight, prepares them into the matrix $Z$, creates some default parameters and runs the filter. Then it prints the likelihood and plots the filter results for $pitch, xdot$ and $x$ along with the measurements.

\section{EM algorithm}

EM algorithm is performed by function \texttt{em(Z, A, H, Q, R, P, x, max\_iter, variable\_params, main\_ind, kappa, kappa2, kappa3)}. 
\begin{itemize}
\item The parameters \texttt{Z, A, H, Q, R, P, x} are identical to the parameters of \texttt{kalman\_filter}, where \texttt{A, H, Q, R} are used as the initial parameters for EM algorithm. 
\item \texttt{max\_iter} controls the maximum number of iterations of the algorithm. 
\item \texttt{variable\_params} is a function handle (pointer to a function) that returns 4 lists: \texttt{[A\_ind,H\_ind,Q\_ind,R\_ind]}. Those are the indexes (to corresponding matrices) of parameters that should be learnt. The rest of parameters in matrices \texttt{A, H, Q, R} will remain fixed as given in the input parameters. For instance, we usually set \texttt{H\_ind} to empty list and \texttt{H} to identity matrix.
\item \texttt{main\_ind} are the indexes to matrix \texttt{A} describing the parameters to which penalization should not be applied (penalization model is described in Section \ref{sec:parameters-of-model}). Penalization of matrix \texttt{A} is implicitly disabled by setting \texttt{kappa} to $0$.
\item \texttt{kappa, kappa2, kappa3} are the tradeoff constants for penalization model of \texttt{A, Q} and \texttt{R}, respectively. $Q_{def}$ and $R_{def}$ are set to be the input parameters \texttt{Q, R}.
\item The function returns the new learnt parameters \texttt{[A,H,Q,R]}.
\end{itemize}

The algorithm internally calls \texttt{kalman\_filter} function (used as smoother), helper methods \texttt{maximize\_A, maximize\_H, maximize\_Q, maximize\_R} to perform the maximization steps and \texttt{convergence(A, H, Q, R, A\_new, H\_new, Q\_new, R\_new)} that decides whether the algorithm has converged. Convergence is here defined as sum of absolute changes of all parameters is below a certain threshold. We set this value to $0.005$, but it may be useful to tune it for other applications.

Function \texttt{log\_likelihood(X\_apr, X\_post, X\_smooth, Z, P\_smooth, A, H, Q, R, L)} computes the expected log likelihood normalized by the size of the data. It is not used by EM algorithm, because it directly uses the maximizing equations and does not need to calculate the actual likelihood value. We can use this function as orientation measure. We can obtain all the input parameters by calling \texttt{kalman\_filter}.

\section {Particle filter}

\subsubsection{Filter and smoother}

Particle filter does not constrain the model to be just matrices, but it can be represented by arbitrary functions. We design the code to separate the main algorithm from the functions defining the particular model. The main functions required by \texttt{particle\_filter} are: (they should be passed as function handles)
\begin{itemize}
\item \texttt{X\_new = transition\_model(u, X, params)} - generates a new set of particles given the current particles as rows of \texttt{X}, current control signal in row vector \texttt{u} and some optional parameters \texttt{params} (cell array).

\item \texttt{W = measurement\_model(z, X, params)} - returns a column vector \texttt{W} of weights for given particles \texttt{X}, row vector of current measurements \texttt{z} and some optional parameters \texttt{params} (cell array). The weights do not have to be normalized.

\item \texttt{X = init\_particles(z, n, M)} - creates the initial set of \texttt{M} particles of size \texttt{n} (state vector size). For generating the particles, it may use the first measurement (column) vector.

\item \texttt{p = transition\_probability(u, X, X\_new)} - does not need to be defined for filter, but is required for smoother. Calculates the probability of transition from \texttt{X} to \texttt{X\_new} given control signals \texttt{u}.
\end{itemize}

The main function for Particle filtering and smoothing is

 \begin{center} \texttt{\justify particle\_filter(Z, u, n, M, transition\_model, measurement\_model, transition\_prob,init\_particles, params, do\_smooth)}. 
 \end{center}
 Boolean parameter \texttt{do\_smooth} enables smoothing. \texttt{Z} is a matrix of all measurement (row for each time step), similarly \texttt{u} for control signals. \texttt{params} is a cell array that is passed to functions \texttt{transition\_model}, \texttt{measurement\_model} and \texttt{transition\_prob}.

It returns the following variables:
\begin{itemize}
\item \texttt{filtered} - a matrix of filtered state estimates (row for each time step)
\item \texttt{smoothed} - a matrix of smoothed state estimates (row for each time step) or \texttt{NaN} if smoother disabled
\item \texttt{Xs} - sets of particles for each time step (matrix $M\times n \times T$) or \texttt{NaN} if smoother disabled
\item \texttt{Ws} - filtered weights for each time step (matrix $M\times n \times T$) or \texttt{NaN} if smoother disabled
\item \texttt{Wss} - smoothed weights for each time step (matrix $M\times n \times T$) or \texttt{NaN} if smoother disabled
\end{itemize}

The output variables are required by function \texttt{pf\_likelihood} that calculates the normalized expected log likelihood.

\subsubsection{Asynchronous filter}

Function \texttt{async\_particle\_filter(events, n, M, transition\_model, measurement\_model, init\_particles, params)} implements the asynchronous version of Particle filter. It accepts \texttt{events} matrix that contains various types of events as its rows. The first item of an event is a number describing its type, \texttt{1} stands for a control signal. In this case, transition step is performed on the current set of particles. Otherwise, measurement step (including resampling) is performed. Measurement model should also distinguish between various kinds of measurements.  In our ground robot example, we use \texttt{measurement\_model\_ground} measurement function, which either assigns the weights for GPS or compass measurements (does not require all measurements at each time).

\subsubsection{Examples}

Script \texttt{run\_task\_I\_PF} demonstrates usage of the Particle filter and plots sample results. It uses functions that define the model in a simple and readable way, e.g.

\begin{center}
\texttt{X\_new(:, xdot\_i) = c0 * X(:, xdot\_i) +  c1 * X(:, pitch\_i);}
\end{center}

On the other hand, model defining functions we use in experiments are more general and expect parameters matrices like Kalman filter in \texttt{params} cell array.

\subsubsection{Measurement models}

To estimate the probability of a measurement $z$ given a particle $x$, our models use a helper function \texttt{gaussian\_probability(z, x, sigma, prec)} that works for a single state variable and multiple particles. \texttt{z} is the measured value, \texttt{x} contains the values of particles (used as the mean of the probability distribution). The function calculates
\begin{equation}
p(z | x) = normcdf(z + prec/2, x, \texttt{sigma}) - normcdf(z - prec/2, x, \texttt{sigma}),
\end{equation}
where $normcdf(z, x, \texttt{sigma})$ is the probability of values less than $z$ for a normal distribution with mean $x$ and variance $\texttt{sigma}^2$. We use various precisions (\texttt{prec}) for various sensors.

\section{Evaluation}

The main function for evaluating methods is \texttt{evaluate} with the following parameters:
\begin{itemize}
\item \texttt{data} - data to be used. The method parses the data and splits into train and test sets as described below.
\item \texttt{train\_size} - if using fixed train size method, used as the train size. Forward chaining uses this as the size of one fold.
\item \texttt{test\_size} - test set size.
\item \texttt{k} - number of iterations. Each iteration uses different train and test set.
\item \texttt{learners} - a cell array with function handles to learning methods to be compared. The function interface is described below.
\item \texttt{chaining} - a boolean parameter enabling forward chaining.
\item \texttt{test\_offset} - distance of the start of test set from the end of train set.
\end{itemize}

Let us denote the size of \texttt{learners} by $L$. The output variables of \texttt{evaluate} are as follows:
\begin{itemize}
\item \texttt{Q\_train} - normalized expected likelihood on the train set for all learners and all iterations (matrix $L \times k$)
\item \texttt{Q\_test} - normalized expected likelihood on the test set for all learners and all iterations (matrix $L \times k$)
\item \texttt{pitch\_pred\_test} - values of predictions of $pitch$ for all learners and all iterations (matrix $\texttt{test\_size} \times L \times k$)
\item \texttt{xdot\_pred\_test} - values of predictions of $xdot$ for all learners and all iterations (matrix $\texttt{test\_size} \times L \times k$)
\item \texttt{pitch\_err} -  MSE of predictions of $pitch$ for all learners and all iterations (matrix $L \times k$)
\item \texttt{xdot\_err} -  MSE of predictions of $xdot$ for all learners and all iterations (matrix $L \times k$)
\item \texttt{taskI\_test} -  filtered values (with known measurements) of $pitch$ and $xdot$ on test set for all learners and all iterations (matrix $2 \times \texttt{test\_size} \times L \times k$)
\item \texttt{test\_data} -  data used for tests (matrix $\texttt{test\_size} \times 12 \times k$, 12 is the number of columns in a single data record).
\end{itemize}

Each \texttt{learner} is expected to implement the following interface. Its only input is the training data. The outputs are
\begin{itemize}
\item \texttt{params} - a cell array of learnt params that will be passed to filtering algorithm and likelihood evaluator.
\item \texttt{filter} - function handle for a function that implements appropriate filtering algorithm for the learner. A \texttt{filter} receives \texttt{params} and test data and outputs predictions for and filtered results $pitch$ and $xdot$. Most of the learners share the common function \texttt{filter\_kalman\_B} or \texttt{filter\_kalman\_C} (depending on the model structure), but there is also the exponential filter implemented in function \texttt{filter\_exponential} and Particle filter in function \texttt{filter\_pf\_all}.
\item \texttt{evaluator} - function handle for a function that calculates the normalized expected likelihood. Common \texttt{evaluator} is \texttt{kalman\_evaluator\_B}, for instance. Particle filter also enables calculating the likelihood, but we usually comment this out and return \texttt{NaN} because the computation is very slow. Exponential filter does not have a method for calculating likelihood.
\end{itemize}

Examples of using the \texttt{evaluate} method and implementing a \texttt{learner} can be found in experiment scripts listed in Section \ref{sec:list-of-scripts}.
\section {List of scripts} \label{sec:list-of-scripts}

\begin{enumerate}
\item \texttt{run\_task\_I\_kalman}
\item \texttt{run\_task\_I\_PF}
\item \texttt{run\_task\_ground\_robots}
\item \texttt{run\_experiment\_distance}
\item \texttt{run\_experiment\_longer\_validation}
\item \texttt{run\_experiment\_scaling}
\item \texttt{run\_experiment\_A}
\item \texttt{run\_experiment\_B}
\item \texttt{run\_experiment\_C}
\item \texttt{run\_experiment\_D}
\item \texttt{run\_experiment\_final\_evaluation}
\end{enumerate}

