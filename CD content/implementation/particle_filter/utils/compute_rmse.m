function [rmse,errors,rmse_per_variable] = compute_rmse(predicted, desired)
    n = size(predicted,2);
    N = size(predicted,1);
    rmse_per_variable = zeros(1, n);
    errors = predicted - desired;
    
    for i=1:n
        rmse_per_variable(1,i) = sqrt(sum(errors(:,i).^2)) / N;
    end
    rmse = sqrt(sum(sum(errors.^2))) / (N*n);