function ind = rand_matrix_indexes(count, m, n)
    row = randi(m, count, 1);
    col = randi(n, count, 1);
    ind = sub2ind([m,n], row, col);