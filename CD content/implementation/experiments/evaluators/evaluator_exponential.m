function [Q, X_post] = evaluator_exponential(params, data)
    %% Exponential filter does not output likelihood
    Q = NaN;
    X_post = NaN;