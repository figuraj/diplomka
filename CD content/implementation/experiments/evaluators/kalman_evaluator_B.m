function [Q, X_post] = kalman_evaluator_B(params, data)
    [Q, X_post] = kalman_evaluator_common(params, data, @get_prior_B);