function [Q, X_post] = pf_evaluator(params, data)
    [pitch,xdot,elevator] = parse_data(data);
    Z = [pitch xdot elevator];
    
    f_mm = @measurement_model_general;
    f_tp = @transition_probability_general;
    f_init = @init_particles_general;
    f_tm = @transition_model_general;
    
    u = 1:size(Z,1);
    M = 1000;
    
    %[filtered,~,Xs,Ws,Wss] = particle_filter(Z,u',3,M,f_tm,f_mm,f_tp,f_init,params,true);
    %Q = pf_likelihood(Xs,Ws,Wss,Z,f_mm,f_tp,u,params);
    %X_post = filtered';
    Q = NaN;
    X_post = NaN;
    % Uncomment to compute the likelihood. Commented due to slow
    % computation.