function [Q, X_post] = kalman_evaluator_C_unscaled(params, data)
    [Q, X_post] = kalman_evaluator_common(params, data, @get_prior_C_unscaled);