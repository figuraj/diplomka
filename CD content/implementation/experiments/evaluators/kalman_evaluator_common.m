function [Q, X_post] = kalman_evaluator_common(params, data, get_prior)
    A = params{1};
    H = params{2};
    Q = params{3};
    R = params{4};
    [x_prior, P] = get_prior();
    
    [pitch,xdot,elevator] = parse_data(data);
    Z = [pitch xdot elevator]';
    
    [X_apr,X_post,X_smooth,~,P_smooth,L] = kalman_filter(Z,A,H,Q,R,P,x_prior,true);
    Q = log_likelihood(X_apr,X_post,X_smooth,Z,P_smooth,A,H,Q,R,L);