function [pitch,xdot,elevator] = parse_data(data)
    [pitch_i,~,elevator_i,~,~,~,~,~,xdot_i,~,~,~] = get_data_indexes();
    pitch = data(:,pitch_i);
    xdot = data(:,xdot_i);
    elevator = data(:,elevator_i);
    