function [pitch_err, xdot_err] = calculate_err(pitch_pred, xdot_pred, data)
    [pitch,xdot,~] = parse_data(data);
    pitch_err = mean((pitch_pred - pitch).^2);
    xdot_err = mean((xdot_pred - xdot).^2);