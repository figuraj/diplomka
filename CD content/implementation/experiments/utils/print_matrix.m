function [] = print_matrix(X, name, tag)
    fprintf('%s: %s = \n', tag, name);
    fprintf([repmat('%f\t', 1, size(X, 2)) '\n'], X');
    fprintf('\n');