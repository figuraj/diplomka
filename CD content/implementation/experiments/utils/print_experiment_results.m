function [] = print_experiment_results(Q_train, Q_test, pitch_err, xdot_err)

Q_train
Q_test

mean_Q_train = mean(Q_train,2)
mean_Q_test = mean(Q_test,2)

pitch_err
xdot_err

mean_pitch_err = mean(pitch_err,2)
mean_xdot_err = mean(xdot_err,2)