function [] = print_params_kalman(params, tag)
    print_matrix(params{1},'A',tag);
    print_matrix(params{2},'H',tag);
    print_matrix(params{3},'Q',tag);
    print_matrix(params{4},'R',tag);