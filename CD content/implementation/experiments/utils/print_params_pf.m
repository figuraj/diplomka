function [] = print_params_pf(params, tag)
    print_matrix(params{1},'A',tag);
    print_matrix(params{2},'G',tag);