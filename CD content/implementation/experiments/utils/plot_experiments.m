function [] = plot_experiments(pitch_pred, xdot_pred, task_I, data, leg, leg2)
    [pitch,xdot,~] = parse_data(data);

    figure;
    plot(pitch_pred);
    hold all;
    plot(pitch, 'c');
    legend(leg{:},'Measured');
    title('Pitch prediction (not using measurement)')
    
    figure;
    plot(xdot_pred, 'LineWidth', 2);
    hold all;
    plot(xdot, 'c');
    legend(leg{:},'Measured');
    title('xdot prediction (not using measurement)')
    
    figure;
    d = zeros(size(task_I,2),size(task_I,3));
    d(:,:) = task_I(1,:,:);
    plot(d);
    hold all;
    plot(pitch, 'c');
    legend(leg2{:},'Measured');
    title('Pitch filtering using measurement.');

    figure;
    d(:,:) = task_I(2,:,:);
    plot(d, 'LineWidth', 2);
    hold all;
    plot(xdot, 'c');
    legend(leg2{:},'Measured');
    title('xdot filtering using measurement.');