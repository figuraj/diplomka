function [params, filter, evaluator] = learner_kalmanA4(train_data)
    %% Learn by robust linear regression from measurements
    [pitch,xdot,elevator] = parse_data(train_data);
    A = learn_transition_robust(pitch,xdot,elevator);
    [H,Q,R,~,~] = kalman_default_params();
    
    params = create_kalman_params(A,H,Q,R);
    filter = @filter_kalman_B;
    evaluator = @kalman_evaluator_B;
    
    print_params_kalman(params, 'Learner A4');