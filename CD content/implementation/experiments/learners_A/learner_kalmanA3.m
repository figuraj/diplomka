function [params, filter, evaluator] = learner_kalmanA3(train_data)
    %% Learn by linear regression with all data smoothed
    [pitch,xdot,elevator] = parse_data(train_data);
    xdot = smooth_data_polyfit(xdot, 10, false);
    pitch = smooth_data_polyfit(pitch, 10, false);
    
    A = learn_transition(pitch,xdot,elevator);
    [H,Q,R,~,~] = kalman_default_params();
    
    params = create_kalman_params(A,H,Q,R);
    filter = @filter_kalman_B;
    evaluator = @kalman_evaluator_B;
    
    print_params_kalman(params, 'Learner A3');