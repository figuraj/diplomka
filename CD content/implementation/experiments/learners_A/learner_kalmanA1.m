function [params, filter, evaluator] = learner_kalmanA1(train_data)
    %% Learn by linear regression from measurements
    [pitch,xdot,elevator] = parse_data(train_data);
    A = learn_transition(pitch,xdot,elevator);
    [H,Q,R,~,~] = kalman_default_params();
    
    params = create_kalman_params(A,H,Q,R);
    filter = @filter_kalman_B;
    evaluator = @kalman_evaluator_B;
    
    print_params_kalman(params, 'Learner A1');