%% Final evaluation of the best models on separate datasets

learners = {@learner_kalmanA2, @learner_kalmanB4, @learner_kalmanB5, @learner_pf1, @learner_kalmanC3};
L = size(learners,2);

train_data = load_and_prepare_data();
train_data = train_data(1:1000,:);
test1 = load_and_prepare_data('let1.txt',true);
test2 = load_and_prepare_data('let2.txt',true);
test3 = load_and_prepare_data('let4.txt',true);

Q_test = zeros(L,3);
pitch_err = zeros(L,3);
xdot_err = zeros(L,3);
pitch_pred_all = zeros(L,800);
xdot_pred_all = zeros(L,800);

for j=1:L
    learner = learners{j};
    [params, filter, evaluator] = learner(train_data);
    Q_test(j,1) = evaluator(params, test1);
    Q_test(j,2) = evaluator(params, test2);
    Q_test(j,3) = evaluator(params, test3);
    
    [pitch_pred_test, xdot_pred_test, ~] = filter(params,test1);
    [pitch_err(j,1), xdot_err(j,1)] = calculate_err(pitch_pred_test', xdot_pred_test', test1);
    pitch_pred_all(j,:) = pitch_pred_test(1:800); %for plot
    xdot_pred_all(j,:) = xdot_pred_test(1:800);
    
    [pitch_pred_test, xdot_pred_test, ~] = filter(params,test2);
    [pitch_err(j,2), xdot_err(j,2)] = calculate_err(pitch_pred_test', xdot_pred_test', test2);
    
    
    [pitch_pred_test, xdot_pred_test, ~] = filter(params,test3);
    [pitch_err(j,3), xdot_err(j,3)] = calculate_err(pitch_pred_test', xdot_pred_test', test3);
end


plot_experiments(pitch_pred_all(:,:)',xdot_pred_all(:,:)',zeros(800,1), ...
        test1(1:800,:),{'A2','B4','B5','Particle filter','C3'},{''});

Q_test
pitch_err
xdot_err