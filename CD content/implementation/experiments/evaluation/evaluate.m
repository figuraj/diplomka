function [Q_train, Q_test, pitch_pred_test, xdot_pred_test, ...
                pitch_err, xdot_err, taskI_test, test_data] = ...
                     evaluate(data, train_size, test_size, k, learners, chaining, test_offset)
format shortG
warning('off','all');

L = size(learners,2);
Q_train = zeros(L,k);
Q_test = zeros(L,k);
pitch_err = zeros(L,k);
xdot_err = zeros(L,k);

pitch_pred_test = zeros(test_size, L,k);
xdot_pred_test = zeros(test_size, L,k);
taskI_test = zeros(2, test_size, L, k);
test_data = zeros(test_size,12,k);

for i=1:k
    if chaining
            train = data(1:i*train_size,:);
    else %fixed train size
            train = data((i-1)*train_size+1:i*train_size,:);
    end
    
    test_start = i*train_size+1+test_offset;
    test = data(test_start:test_start+test_size-1,:);
    
    test_data(:,:,i) = test;
    
    for j=1:L
        learner = learners{j};
        [params, filter, evaluator] = learner(train);
        
        Q_train(j,i) = evaluator(params, train);
        Q_test(j,i) = evaluator(params, test);
        
        [pitch_pred_test(:,j,i), xdot_pred_test(:,j,i), taskI_test(:,:,j,i)] = filter(params,test);
        [pitch_err(j,i), xdot_err(j,i)] = ...
            calculate_err(pitch_pred_test(:,j,i), xdot_pred_test(:,j,i), test);
    end   
end