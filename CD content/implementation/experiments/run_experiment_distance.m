%% Experiment for comparing errors of validation sets with various distances from the training set

clc;
learners = {@learner_kalmanA2, @learner_kalmanB5, @learner_kalmanC2, @learner_kalmanC3};
L = size(learners,2);
k = 5;
test_size = 1000;
train_size = 1000;
data = load_and_prepare_data();

N = 5;
dx = 1500;
err_xdot_all = zeros(N,L);
err_pitch_all = zeros(N,L);

for i=1:N
    dist = (i-1) * dx;
    [Q_train, Q_test, pitch_pred_test, xdot_pred_test, ...
       pitch_err, xdot_err, X_tskI_test, ~] = evaluate(data,train_size,test_size,k,learners,false,dist);
    err_xdot_all(i,:) = mean(xdot_err,2)';
    err_pitch_all(i,:) = mean(pitch_err,2)';
end

figure;
plot(0:dx:(N-1)*dx,err_xdot_all);
legend('A2','B5','C2','C3');
title('pitch prediction error for increasing distance from training set.');

figure;
plot(0:dx:(N-1)*dx,err_pitch_all);
legend('A2','B5','C2', 'C3');
title('xdot prediction error for increasing distance from training set.');