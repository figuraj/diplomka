function [x, P] = get_prior_C()
    x = [0; 0; 0; 0];
    P = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1];