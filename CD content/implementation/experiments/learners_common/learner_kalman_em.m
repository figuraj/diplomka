function [params, filter, evaluator] = learner_kalman_em(train_data, var_params, tag, kappa)
    %% Learn by EM algoritm
    [pitch,xdot,elevator] = parse_data(train_data);
    xdot_s = smooth_data_polyfit(xdot, 10, false);
    A = learn_transition(pitch,xdot_s,elevator);
    [H,Q,R,x_prior,P] = kalman_default_params();
    Z = [pitch xdot elevator]';
    main_ind = [1 2 5 7];
    if nargin < 4
        kappa = [80;80;80];
    end
    [A,H,Q,R] = em(Z,A,H,Q,R,P,x_prior,20,var_params,main_ind,kappa(1),kappa(2),kappa(3));
    A(2,2) = min(A(2,2),0.997);
    
    params = create_kalman_params(A,H,Q,R);
    filter = @filter_kalman_B;
    evaluator = @kalman_evaluator_B;
    
    print_params_kalman(params, tag);