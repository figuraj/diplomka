function A = learn_transition_robust(pitch,xdot,elevator)
    N = size(pitch,1);
    
    xdot_prev = xdot(1:N-1);
    xdot = xdot(2:N);
    pitch_prev = pitch(1:N-1);
    pitch = pitch(2:N);
    elevator_prev = elevator(1:N-1);
    
    X = [pitch_prev elevator_prev];
    beta = robustfit(X,pitch,'bisquare',4.685,'off');
    p1 = beta(1);
    p0 = beta(2);
    
    X = [xdot_prev pitch_prev];
    beta = robustfit(X,xdot,'bisquare',4.685,'off');
    p3 = beta(1);
    p2 = beta(2);
    p3 = min(p3,0.997);

    %pitch, xdot, elevator
    A = [p1 0 p0; p2 p3 0; 0 0 1];