function [x, P] = get_prior_B()
    x = [0; 0; 0];
    P = [1 0 0; 0 1 0; 0 0 1];