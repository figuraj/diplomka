function [params, filter, evaluator] = learner_kalman_em_C_unscaled(train_data, var_params,tag,kappa)
    %% Learn by EM algoritm
    [pitch,xdot,elevator] = parse_data(train_data);
    xdot_s = smooth_data_polyfit(xdot, 10, false);
    A = learn_transition_C(pitch,xdot_s,elevator);
    [H,Q,R,x_prior,P] = kalman_default_params_C_unscaled();
    Z = [pitch xdot elevator]';
    main_ind = [1 4 5 9 16 6 14];
    if nargin < 4
        kappa = [80;80;0];
    end
    [A,H,Q,R] = em(Z,A,H,Q,R,P,x_prior,20,var_params,main_ind,kappa(1),kappa(2),kappa(3));
    A(2,2) = min(A(2,2),1);
    
    params = create_kalman_params(A,H,Q,R);
    filter = @filter_kalman_C_unscaled;
    evaluator = @kalman_evaluator_C_unscaled;
    
    print_params_kalman(params, tag);