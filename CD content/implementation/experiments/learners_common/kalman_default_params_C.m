function [H,Q,R,x,P] = kalman_default_params_C()
    sigma_pitch = 0.001;
    sigma_xdot = 0.001;
    sigma_elevator = 1;
    sigma_xdotdot = 0.001;
    m_sigma_pitch = 0.01;
    m_sigma_xdot = 0.01;
    m_sigma_elevator = 0.01;
    
    H = [1 0 0 0; 0 1 0 0; 0 0 1 0];

    Q = [sigma_pitch 0 0 0; 0 sigma_xdot 0 0; 0 0 sigma_elevator 0; 0 0 0 sigma_xdotdot];
    R = [m_sigma_pitch 0 0; 0 m_sigma_xdot 0; 0 0 m_sigma_elevator];

    [x,P] = get_prior_C();