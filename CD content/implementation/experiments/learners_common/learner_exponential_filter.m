function [params, filter, evaluator] = learner_exponential_filter(train_data)
    params = learner_kalmanA2(train_data);
    filter = @filter_exponential;
    evaluator = @evaluator_exponential;