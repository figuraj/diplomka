function [H,Q,R,x,P] = kalman_default_params()
    sigma_pitch = 0.001;
    sigma_xdot = 0.001;
    sigma_elevator = 1;
    m_sigma_pitch = 0.01;
    m_sigma_xdot = 0.018;
    m_sigma_elevator = 0.01;
    
    H = [1 0 0; 0 1 0; 0 0 1];

    Q = [sigma_pitch 0 0; 0 sigma_xdot 0; 0 0 sigma_elevator];
    R = [m_sigma_pitch 0 0; 0 m_sigma_xdot 0; 0 0 m_sigma_elevator];
%    Q=[0.000714	0.000078	0.000102	;...
%0.000078	0.001219	0.000044	;...
%0.000102	0.000044	0.089694];
%    R =[0.001675	-0.000215	-0.000045	;...
%-0.000215	0.019187	0.003125	;...
%-0.000045	0.003125	0.007915	
%        ];
    
    [x,P] = get_prior_B();