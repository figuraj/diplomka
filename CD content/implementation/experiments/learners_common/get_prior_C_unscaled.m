function [x, P] = get_prior_C_unscaled()
    x = [0; 0; 0; 0];
    P = [10 0 0 0; 0 1 0 0; 0 0 10 0; 0 0 0 1];