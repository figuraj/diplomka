function A = learn_transition(pitch,xdot,elevator)
    N = size(pitch,1);
    
    pitch_prev = pitch(1:N-1);
    pitch = pitch(2:N);
    elevator_prev = elevator(1:N-1);
    
    X = [pitch_prev elevator_prev];
    beta = regress(pitch,X);
    p1 = beta(1);
    p0 = beta(2);
    
    %pitch, xdot, elevator, xdotdot
    A = [p1 0 p0 0; 0 1 0 1; 0 0 1 0; 0.05 0 0 0.98];