%% Experiment to compare performance of EM algorithm on scaled vs non-scaled data.

clc;
learners = {@learner_kalmanC3};
k = 5;
test_size = 1200;
train_size = 1000;

data = load_and_prepare_data('let3.txt', true);

disp('Scaled data:')
[Q_train, Q_test, pitch_pred_test, xdot_pred_test, ...
       pitch_err, xdot_err, taskI_test] = evaluate(data, train_size,test_size, k, learners,false,1000);


print_experiment_results(Q_train, Q_test, pitch_err, xdot_err);

learners = {@learner_kalmanC3_unscaled};
data = load_and_prepare_data('let3.txt', false);

disp('Non-scaled data:')
[Q_train, Q_test, pitch_pred_test, xdot_pred_test, ...
       pitch_err, xdot_err, taskI_test] = evaluate(data, train_size,test_size, k, learners,false,1000);


print_experiment_results(Q_train, Q_test, pitch_err, xdot_err);
