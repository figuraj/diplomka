function X = init_particles_ground(M,n)
    X = ones(M,n);
    minX = -150;
    maxX = 100;
    minY = 0;
    maxY = 100;
    minComp = 0;
    maxComp = 2 * pi;
    
    X(:,1) = minX + (maxX-minX) * rand(M,1);
    X(:,2) = minY + (maxY-minY) * rand(M,1);
    X(:,3) = minComp + (maxComp-minComp) * rand(M,1);
    
        