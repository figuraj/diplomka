gps_data = load('robot_gps.txt');
x_real = gps_data(:,1);
y_real = gps_data(:,2);
gps_x = gps_data(:,4);
gps_y = gps_data(:,5);
gps_accuracy = gps_data(:,6);
N = size(gps_data,1);

figure;
plot(x_real,y_real, gps_x, gps_y);
legend('Real position', 'GPS');
title('Position');

gps_err = sqrt((x_real - gps_x).^2 + (y_real - gps_y).^2);

figure;
plot(1:N,gps_err,1:N,gps_accuracy);
legend('Distance error GPS', 'GPS expected accuracy');
title('GPS error in time');

figure;
subplot(3,1,1);
hist(gps_err,200);
title('GPS distance error histogram');

gps_x_err = x_real - gps_x;
subplot(3,1,2);
hist(gps_x_err,200);
title('GPS x axis error histogram');

gps_y_err = y_real - gps_y;
subplot(3,1,3);
hist(gps_y_err,200);
title('GPS y axis error histogram');