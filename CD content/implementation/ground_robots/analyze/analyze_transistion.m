moves_data = load('robot_moves.txt');
N = size(moves_data,1);
X = moves_data(:,1:3);
X_new = zeros(N-1,3);
for i=1:N-1
    alpha = moves_data(i,4);
    dist = moves_data(i,6);
    beta = moves_data(i,8);
    X_new(i,:) = transition_deterministic_ground(alpha,dist,beta,X(i,:));
end

pos_x_err = X(2:N,2) - X_new(:,2);
pos_y_err = X(2:N,3) - X_new(:,3);
figure;
plot(pos_x_err);
legend('Position error x axis');
title('Transition model position error in time')

orient_err = X_new(:,3) - X(2:N,3);
figure;
plot(orient_err);
legend('Orientation error');
title('Transition model orientation error in time')

figure;
subplot(3,1,1);
histfit(pos_x_err,200);
title('Transition model position error x axis histogram')

subplot(3,1,2);
histfit(pos_y_err,200);
title('Transition model position error y axis histogram')

subplot(3,1,3);
histfit(orient_err,200);
title('Transition model orientation error histogram');