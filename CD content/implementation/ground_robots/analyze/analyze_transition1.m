moves_data = load('robot_moves.txt');
N = size(moves_data,1);
X = moves_data(:,1:3);
X_new = zeros(N-1,3);
for i=1:N-1
    alpha = moves_data(i,4);
    dist = moves_data(i,6);
    beta = moves_data(i,8);
    X_new(i,:) = transition_deterministic_ground(alpha,dist,beta,X(i,:));
end

dist = moves_data(1:N-1,6);
alpha = moves_data(1:N-1,4);
beta = moves_data(1:N-1,8);

x_real = X(2:N,1);
y_real = X(2:N,2);
angle_real = X(2:N,3);

x_estim = X_new(:,1);
y_estim = X_new(:,2);
angle_estim = X_new(:,3);

angle_err = abs(angle_estim - angle_real);
b = [dist abs(alpha + beta)] \ angle_err
estim_angle_err = [dist abs(alpha + beta)] * b;
accuracy = mean(abs(estim_angle_err - angle_err))

x_err = abs(x_estim - x_real);
b = [dist dist.^2] \ x_err
estim_x_err = [dist dist.^2] * b;
accuracy = mean(abs(estim_x_err - x_err))

y_err = abs(y_estim - y_real);
b = [dist dist.^2] \ y_err
estim_y_err = [dist dist.^2] * b;
accuracy = mean(abs(estim_y_err - y_err))

figure;
xplot = 1:1000;
plot(xplot, dist(xplot), xplot,alpha(xplot) + 0.3, xplot, angle_err(xplot), xplot, estim_err(xplot));
legend('Control signal distance', 'Control signal alpha', 'Position error', 'Estim err');