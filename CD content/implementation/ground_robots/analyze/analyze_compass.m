compass_data = load('robot_compass.txt');
N = size(compass_data,1);
orientation_real = compass_data(:,3);
compass = compass_data(:,4);
compass(compass < 2) = compass(compass < 2) + 2*pi;
figure;
subplot(2,1,1);
plot(1:N, orientation_real, 1:N, compass);
grid on;
legend('Real orientation', 'Compass');
title('Orientation in time');

orientation_real = mod(orientation_real,2*pi);
compass = mod(compass, 2*pi);
err_compass = zeros(N,1);
comp_min = min(orientation_real, compass);
comp_max = max(orientation_real, compass);
for i=1:N
    if (comp_max(i) - comp_min(i)) < (comp_min(i) + 2*pi - comp_max(i))
        err_compass(i) = - (orientation_real(i) - compass(i));
    else
        err_compass(i) = - (2 * pi + orientation_real(i) - compass(i));
    end
end

%err_compass = min(comp_max - comp_min, comp_min + 2*pi - comp_max);
subplot(2,1,2);
plot(err_compass);
legend('Compass error');
title('Compass error in time');
grid on;
figure;
hist(err_compass,100);
title('Compass error histogram');