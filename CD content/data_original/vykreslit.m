%% Author: Tomas Baca

clear

data = load('let1_23.09.2013.txt');

%% definice sloupcu v datech

pitch = 1;
roll = 2;
elevator = 3;
aileron = 4;
throttle = 5;
x = 6;
y = 7;
yaw = 8;
x_dot = 9;
y_dot = 10
z = 11;
time = 12;

%% vyrez

from = 1;
to = 800;

%% plotting osa dopredu/dozadu

figure(1)

subplot(4, 1, 1);
plot(data(from:to, time), filtr_extremnich_hodnot(data(from:to, x)));
title('Poloha v ose X');
xlabel('Cas [s]');
ylabel('Poloha []');

subplot(4, 1, 2);
plot(data(from:to, time), filtr_extremnich_hodnot(data(from:to, x_dot)));
title('Rychlost v ose X');
xlabel('Cas [s]');
ylabel('Rychlost [m/s]');

subplot(4, 1, 3);
plot(data(from:to, time), filtr_extremnich_hodnot(data(from:to, pitch)));
title('Naklon v ose X');
xlabel('Cas [s]');
ylabel('Uhel [deg]');

subplot(4, 1, 4);
plot(data(from:to, time), data(from:to, elevator));
title('Ridici signal v ose X (pozadovany naklon)');
xlabel('Cas [s]');
ylabel('Uhel [deg]');

%% osa doleva/doprava

figure(2)

subplot(4, 1, 1);
plot(data(from:to, time), filtr_extremnich_hodnot(data(from:to, y)));
title('Poloha v ose Y');
xlabel('Cas [s]');
ylabel('Poloha []');

subplot(4, 1, 2);
plot(data(from:to, time), filtr_extremnich_hodnot(data(from:to, y_dot)));
title('Rychlost v ose Y');
xlabel('Cas [s]');
ylabel('Rychlost [m/s]');

subplot(4, 1, 3);
plot(data(from:to, time), filtr_extremnich_hodnot(data(from:to, roll)));
title('Naklon v ose Y');
xlabel('Cas [s]');
ylabel('Uhel [deg]');

subplot(4, 1, 4);
plot(data(from:to, time), data(from:to, aileron));
title('Ridici signal v ose Y (pozadovany naklon)');
xlabel('Cas [s]');
ylabel('Uhel [deg]');

%% zbyla data

figure(3)

subplot(3, 1, 1);
plot(data(from:to, time), filtr_extremnich_hodnot(data(from:to, z)));
title('Vyska vzhledem k zemi');
xlabel('Cas [s]');
ylabel('Vyska [m]');

subplot(3, 1, 2);
plot(data(from:to, time), data(from:to, throttle));
title('Ridici signal vysky');
xlabel('Cas [s]');
ylabel('Vykon motoru [% max otacek]');

subplot(3, 1, 3);
plot(data(from:to, time), filtr_extremnich_hodnot(data(from:to, yaw)));
title('Uhel Yaw (rotace kolem svisle osy)');
xlabel('Cas [s]');
ylabel('Uhel [deg]');

